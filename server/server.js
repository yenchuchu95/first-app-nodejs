var express = require('express');
var bodyParser = require('body-parser');
var {ObjectID} = require('mongodb');

var {mongoose} = require('./db/mongoose');
var {Todo} = require('./models/todo');
var {User} = require('./models/user');

var app = express();

app.use(bodyParser.json());

//req: dữ liệu phần requests
// res: dữ liệu được trả về phần response


app.get('/todos/:id', (req, res) => {
    var id = req.params.id;
    console.log(id);
    if(!ObjectID.isValid(id)) {
       return res.status(404).send();
    }

    // Todo.findById(id).then((todos) => {
    //     res.send({todos});
    // }, (e) => {
    //     res.status(400).send(e);
    // })

    Todo.findById(id).then((todo) => {
        if(!todo) {
            return res.status(404).send();
        }
        console.log(todo);
        res.send(todo);
    }).catch((e) => {
        res.status(400).send();
    });


});

app.post('/todos', (req, res) => {
    var todo = new Todo({
        text: req.body.text
    });

    todo.save().then((doc) => {
        res.send(doc); // response trả về sau khi chạy xong
    }, (e) => {
        res.status(400).send(e);
    });
});

app.get('/todos', (req, res) => {
    Todo.find().then((todos) => {
        res.send({todos});
    }, (e) => {
        res.status(400).send(e);
    })
});

app.listen(3000, () => {
    console.log('Started on port 3000');
});

module.exports = {app};