const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if(err) {
        return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');

    // delete Many
    // db.collection('Users').deleteMany({'name': 'No'}).then((result) => {
    //     console.log(result);
    // });

    // delete One
    // db.collection('Users').deleteOne({'name': 'test'}).then((result) => {
    //     console.log(result);
    // });

    // find one and update
     db.collection('Users').findOneAndUpdate(
        {
             'age': 30
        },
        {
            $set: {
            'name': 'Hine test 233444'
            },
            $inc: {
                'age': 5
            }
        },
        {
            returnOriginal: false
        }
    ).then((result) => {
         console.log(result);
     });

db.close();

});